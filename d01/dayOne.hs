module DayOne where

import System.IO
import Data.Maybe

inputFileName = "input.txt"

input :: String -> [Integer]
input contents = toNum (cleanNumbers (splitNumbers contents))
    where 
        cleanNumbers strs = map removeLeadingPlus strs
        readNum str = read str 
        splitNumbers string = words string
        toNum numbers = map readNum numbers
        removeLeadingPlus str = if str !! 0 == '+' then tail str else str

-- First Part

sumNumbers :: Num a => [a] -> a
sumNumbers numbers = sum numbers

partOne :: IO ()
partOne = do
    contents <- readFile inputFileName
    print (sumNumbers (input contents))

-- Second Part

accumulate :: [Integer] -> [Integer]
accumulate list = accumulator list 0
    where
        accumulator :: Num a => [a] -> a -> [a]
        accumulator (x:xs) state = (x + state) : accumulator xs (x + state)
        accumulator [] state = []

duplicate :: [Integer] -> Maybe Integer
duplicate list = dup list [0]
    where
        dup (x:xs) visited =
            if elem x visited
                then Just x
                else dup xs (x:visited)
        dup [] _ = Nothing

firstCyclicDuplicate :: [Integer] -> Maybe Integer
firstCyclicDuplicate list = duplicate (accumulate cycledList)
    where
        cycledList = cycle list

main :: IO ()
main = do
    contents <- readFile inputFileName
    -- print $ take (5 * (10 ^ 5)) $ accumulate $ cycle $ input contents
    case (firstCyclicDuplicate (input contents)) of
        Just a -> print (show a)
        Nothing -> print "There is no first duplicate"
