# Advent of Code 2018

This are my solutions to the [Advent of Code 2018](https://adventofcode.com/2018/) quizzes. I try to solve them all in Haskell but I will fall back to Python if the quiz is just a bit too difficult to solve in Haskell.