module DayTwo where

    import Data.List

    inputFileName :: String
    inputFileName = "input.txt"

    input :: String -> [String]
    input str = words str

    -- First Part

    charFrequency :: String -> [(Char, Int)]
    charFrequency list = map (\x -> (head x, length x)) (group (sort list))

    getNOccuringChars :: Int -> String -> [(Char, Int)]
    getNOccuringChars n str = filter (\x -> (snd x) == n) (charFrequency str)

    hasNChar :: Int -> String -> Bool
    hasNChar n str = (getNOccuringChars n str) /= []

    numOfNOccurenceChars :: [String] -> Int -> Integer
    numOfNOccurenceChars strList n = sum (map (\x -> if x then 1 else 0) (map (hasNChar n) strList))

    checkSum :: [String] -> Integer
    checkSum inp = let 
        numOfDoubleOccurences = numOfNOccurenceChars inp 2
        numOfTripleOccurences = numOfNOccurenceChars inp 3
        in numOfDoubleOccurences * numOfTripleOccurences

    partOne :: IO ()
    partOne = do
        contents <- readFile inputFileName
        print ("The checksum is: " ++ (show (checkSum (input contents))))

    -- Part Two

    correctBoxes :: [String] -> Maybe (String, String)
    correctBoxes boxes = iterate' boxes
        where
            isCorrectBox box = filter (\y -> (length (y \\ box)) == 1) boxes
            iterate' (x:xs) = if isCorrectBox x /= [] then Just (head (isCorrectBox x), x) else iterate' xs
            iterate' [] = Nothing

    partTwo :: IO ()
    partTwo = do
        contents <- readFile inputFileName
        case correctBoxes (input contents) of
            Just boxes -> print (intersect (fst boxes) (snd boxes))
            Nothing -> print "There were no correct boxes"
